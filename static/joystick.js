
    'use strict';

    window.requestAnimFrame = (function (window) {
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame
    })(window);


    function JoyStick(){

        var canvas = null;
        var context = null;
        var stick = null;
        var pointerDown = false;
        var lastX = null;
        var lastY = null;
        var stickRadius = 30;
        var origo = null;

        function isPointerIntersectingStick(pointerX, pointerY){
            return pointerX >= stick.x-stickRadius && pointerX <= stick.x+stickRadius &&
                pointerY >= stick.y-stickRadius && pointerY <= stick.y+stickRadius;
        }

        function onPointerDown(e) {
            lastX = e.clientX;
            lastY = e.clientY;
            if(isPointerIntersectingStick(e.clientX, e.clientY))
                pointerDown = true;
        }

        function onPointerMove(e) {
            if(pointerDown){
                var x =  e.clientX - lastX;
                var y = e.clientY - lastY;
                lastX = e.clientX;
                lastY = e.clientY;
                stick.x += x;
                stick.y += y;
            }
        }

        function onPointerUp(e) {
            pointerDown = false;
        }

        function moveStickTowardsOrigo(){
            // TODO: This can most likely be done much nicer
            // and with a better effect
            if(stick.x !== origo.x || stick.y !== origo.y) {
                var angle = Math.atan(Math.abs(stick.y-origo.y) / Math.abs(stick.x - origo.x));
                var distance = Math.abs(stick.y-origo.y) / Math.sin(angle);
                var ny = Math.sin(angle) * (distance-0.1);
                var nx = Math.cos(angle) * (distance-0.1);

                if(stick.x !== origo.x){
                    stick.x = stick.x > origo.x ? stick.x - nx : stick.x + nx;
                }

                if(stick.y !== origo.y){
                    stick.y = stick.y > origo.y ? stick.y - ny : stick.y + ny;
                }

            }
        }

        function draw() {

            if(!pointerDown)
                moveStickTowardsOrigo();

            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = '#333366';
            context.strokeStyle = 'black';
            context.rect(0,0,canvas.width,canvas.height);
            context.fill();
            context.stroke();


            context.beginPath();
            context.fillStyle = "#333366";
            context.beginPath();
            context.strokeStyle = stick.color;
            context.fillStyle = 'black'
            context.lineWidth = "6";
            context.arc(stick.x, stick.y, stickRadius, 0, Math.PI * 2, true);
            context.stroke();
            context.fill();

            window.requestAnimFrame(draw);
        }

        this.setup = function(elementId, width, height){
            canvas = document.getElementById(elementId);
            context = canvas.getContext('2d');
            canvas.width = width;
            canvas.height = height;

            origo = {
                x: width/2,
                y: height/2
            };

            stick = {
                x: origo.x,
                y: origo.y,
                color: 'grey'
            }

            canvas.addEventListener('pointerdown', onPointerDown, false);
            canvas.addEventListener('pointermove', onPointerMove, false);
            canvas.addEventListener('pointerup', onPointerUp, false);
            //canvas.addEventListener('pointerout', onPointerUp, false);
            window.requestAnimFrame(draw);
        };

    };

