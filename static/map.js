window.RoverMap = (function(window){
    'use strict';

    var RoverMap = function() {
        var map = null;
        var canvas = null;
        var waypoints = [];
        var marker = null;

        this.init = function(canvasElement){
            canvas = canvasElement;
        };

        this.onload = function(){
            map = new google.maps.Map(document.getElementById(canvas), {
                zoom: 16,
                center: {lat: 56.846982, lng: 14.822474},
                disableDefaultUI: true,
                zoomControl: true,
                mapTypeControl: true
            });

            marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 5
                },
                draggable: false,
                map: map
            });
        };

        this.moveMarker = function(latlang){
            if(latlang == null)
                return;

            marker.setPosition(latlang);
            map.setCenter(latlang);
        }

        window.google.maps.event.addDomListener(window, 'load', this.onload);

    };

    return RoverMap;

})(window);