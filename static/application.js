window.Application = (function(window, $, RoverMap){

    return function () {
        var self = this;
        var socket = null;
        var map = null;

        function setupSocket(){
            var socketAddress = 'ws://'+window.location.hostname+'/control'
            socket = new WebSocket(socketAddress);
            socket.onopen = function(){}
            socket.onmessage = function(){
                function formatNumber(s, n) {
                    return parseFloat(s).toFixed(n || 2);
                }

                function formatIsoDate(d){
                    return new Date(d).toLocaleString()
                }

                function getGpsStatus(n) {
                    switch(n){
                        case "1": return "No fix";
                        case "2": return "2D fix";
                        case "3": return "3D fix";
                        default: return "No status";
                    }
                }

                var message = JSON.parse(e.data);
                switch(message.action){
                    case 'proximity-update':
                        $('#distance').html(formatNumber(message.distance) + ' cm');
                        $('#temperature').html(formatNumber(message.temperature, 1) + ' &deg;C');
                        $('#pressure').html(formatNumber(message.pressure) + ' mBar');
                        $('#baltitude').html(formatNumber(message.altitude) + ' m');
                        break;
                    case 'log':
                        break;
                    case 'gps-update':

                        map.moveMarker( {
                            'lng': message.data.lng,
                            'lat': message.data.lat
                        });

                        $('#gps-time').html(formatIsoDate(message.data.utc_date));
                        $('#coordinates').html(message.data.lng + ',' + message.data.lat);
                        $('#direction').html(message.data.direction + ' &deg;');
                        $('#galtitude').html(message.data.altitude + ' m');
                        $('#speed').html(formatNumber(message.data.ground_speed, 2) + ' m/s');
                        $('#gps_status').html(getGpsStatus(message.data.status) + ' (' + message.data.num_sats+ ')');
                        break;
                    default:
                        console.log('Unknown command')

                }

            }
        }

        function setupEngineControls(){
            $("input[type='range']").on("input", function(){
                $(this).prev('output').html($(this).val() + ' %');
                socket.send({
                    'action': 'engine-speed',
                    'engine': $(this).attr('data-engine'),
                    'speed': parseFloat($(this).val() / 100)
                });
            });
        }

        function setupMap(){
            map = new RoverMap();
            map.init('map-canvas');
        }

        function setupVideoControl(){
            $("#video").attr('src',  'http://'+window.location.hostname+':8080/stream/video.mjpeg');
        }

        function setupTabs(){
            $(".tabs-menu a").click(function(event) {
                console.log('Tab clicked');
                event.preventDefault();
                $(this).parent().addClass("current");
                $(this).parent().siblings().removeClass("current");
                var tab = $(this).attr("href");
                $(".tab-content").not(tab).css("display", "none");
                $(tab).fadeIn(200);
            });
        }

        self.setup = function(){
            setupTabs();
            setupEngineControls();
            setupVideoControl();
            setupSocket();
            setupMap();
        };
    };

})(window, $, RoverMap);