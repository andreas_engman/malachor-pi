import multiprocessing
import time
import datetime
import config
import serial
import pynmea2
import logging


class GpsPoller(multiprocessing.Process):
    """
    Polling the gps for new sentances. For more information on setting up the gps chip, see

        https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi/setting-everything-up
        https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi/using-uart-instead-of-usb
        http://www.stuffaboutcode.com/2013/09/raspberry-pi-gps-setup-and-python.html
        http://www.gpsinformation.org/dale/nmea.htm

    """
    port = None
    output_queue = None

    def __init__(self, output_queue):
        multiprocessing.Process.__init__(self)
        self.port = serial.Serial("/dev/ttyAMA0", baudrate=config.UART_BAUD_RATE, timeout=config.UART_TIMEOUT)
        self.output_queue = output_queue

    def close(self):
        if None != self.port:
            self.port.close()

    def _should_queue_message(self, message):
        supported_sentences = ["GSA", "GSV", "RMC"]
        for s in supported_sentences:
            if type(message).__name__ == s:
                return True
        return False

    def _create_package(self, gga, gsa, rmc):
        return {
            'action': 'gps-update',
            'data': {
                'status': gsa.mode_fix_type,
                'pdop': gsa.pdop,
                'hdop': gsa.hdop,
                'vdop': gsa.vdop,
                'lat': gga.latitude,
                'lng': gga.longitude,
                'age_gps_data': gga.age_gps_data,
                'num_sats': gga.num_sats,
                'altitude': gga.altitude,
                'datestamp': rmc.datestamp.isoformat(),
                'timestamp': rmc.timestamp.isoformat(),
                'utc_date': datetime.datetime.combine(rmc.datestamp, rmc.timestamp).isoformat(),
                'ground_speed': rmc.spd_over_grnd,
                'direction': rmc.true_course
            }
        }

    def _has_complete_package_info(self, gga, gsa, rmc):
        return None != gga and None != gsa and None != rmc

    def _put_on_queue(self, gga, gsa, rmc):
        package = self._create_package(gga, gsa, rmc)
        self.output_queue.put(package)

    def run(self):
        current_timestamp = None
        current_gga = None
        current_gsa = None
        current_rmc = None

        while True:
            line = self.port.readline()

            try:
                message = pynmea2.parse(line)
            except:
                logging.warning("Unable to parse line %s", line)
                continue

            if hasattr(message, 'timestamp'):
                # reset, wait for new three sentences of interest
                if current_timestamp != message.timestamp:
                    current_timestamp = message.timestamp
                    current_gga = None
                    current_gsa = None
                    current_rmc = None

            typeof_sentence = type(message).__name__
            if typeof_sentence == 'GGA':
                current_gga = message
            elif typeof_sentence == 'GSA':
                current_gsa = message
            elif typeof_sentence == 'RMC':
                current_rmc = message

            if self._has_complete_package_info(current_gga, current_gsa, current_rmc):
                self._put_on_queue(current_gga, current_gsa, current_rmc)

            time.sleep(0.1)
