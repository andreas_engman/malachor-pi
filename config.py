"""
Place of all magic numbers and definitions
"""
SPEED_OF_SOUND_MM_PER_SEC = 34300
DISTANCE_SENSOR_BCM_TRIGGER_PIN = 23
DISTANCE_SENSOR_BCM_ECHO_PIN = 24
HEADLIGHTS_BCM_PIN = -1
BUZZER_BCM_OIN = -1
SENSORS_POLL_INTERVAL = 1
GPS_POLL_INTERVAL = 1
UART_BAUD_RATE = 9600
UART_TIMEOUT = 3.0
