__author__ = 'andreasengman'

import RPi.GPIO as GPIO
import time

TRIG = 23
ECHO = 24


def setup():
    print "Setup"
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(TRIG, GPIO.OUT)
    GPIO.setup(ECHO, GPIO.IN)
    GPIO.output(TRIG, False)
    time.sleep(2)
    print "Done!"

def main():
    setup()
    try:
        while True:
            GPIO.output(TRIG, True)
            GPIO.output(TRIG, False)

            while GPIO.input(ECHO) == 0:
                pulse_start = time.time()

            while GPIO.input(ECHO) == 1:
                pulse_end = time.time()

            pulse_duration = pulse_end - pulse_start
            distance = round(pulse_duration * 1715000, 0)
            print("Distance: ", distance, "mm")
            time.sleep(1)
    except KeyboardInterrupt:
        print("Keyboard interupt")
        GPIO.cleanup()
