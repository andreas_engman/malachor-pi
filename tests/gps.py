import serial


class SerialPoller(object):
    serial = None

    def __init__(self):
        self.port = serial.Serial("/dev/ttyAMA0", baudrate=9600, timeout=3.0)

    def run(self):
        while True:
            line = self.port.read(18)
            print line

    def close(self):
        if None != self.port:
            self.port.close()


def main():
    poller = SerialPoller()
    try:
        poller.run()
    except:
        poller.close()


if __name__ == "__main__":
    main()
