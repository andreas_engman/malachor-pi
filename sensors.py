import RPi.GPIO as GPIO
import Adafruit_BMP.BMP085 as BMP085
import logging
import config
import time
import multiprocessing

class SensorPoller(multiprocessing.Process):

    distanceSensor = None
    bmpSensor = None

    def __init__(self, output_queue):
        multiprocessing.Process.__init__(self)
        self.output_queue = output_queue

        logging.info("Setting mode to BCM")
        GPIO.setmode(GPIO.BCM)

        self.distanceSensor = DistanceSensor()
        self.bmpSensor = BmpSensor()

    def close(self):
        GPIO.cleanup()

    def run(self):
        while True:
            distance, pulse_duration = self.distanceSensor.get_values()
            temperature, pressure, altitude, sealevel_pressure = self.bmpSensor.get_values()
            message = {
                'action': 'proximity-update',
                'distance': distance,
                'temperature': temperature,
                'pressure': pressure,
                'altitude': altitude,
                'sealevel_pressure': sealevel_pressure
            }

            self.output_queue.put(message)
            time.sleep(config.SENSORS_POLL_INTERVAL)


class DistanceSensor(object):
    def __init__(self):
        logging.info("Setting up HC-SR04 on pin %d (trigger) and %d (echo)", config.DISTANCE_SENSOR_BCM_ECHO_PIN,
                     config.DISTANCE_SENSOR_BCM_TRIGGER_PIN)

        GPIO.setup(config.DISTANCE_SENSOR_BCM_TRIGGER_PIN, GPIO.OUT)
        GPIO.setup(config.DISTANCE_SENSOR_BCM_ECHO_PIN, GPIO.IN)
        GPIO.output(config.DISTANCE_SENSOR_BCM_TRIGGER_PIN, False)

        logging.info("Waiting for Sensor To settle")

        time.sleep(2)

    def get_values(self):
        GPIO.output(config.DISTANCE_SENSOR_BCM_TRIGGER_PIN, True)
        time.sleep(0.00001)
        GPIO.output(config.DISTANCE_SENSOR_BCM_TRIGGER_PIN, False)

        while GPIO.input(config.DISTANCE_SENSOR_BCM_ECHO_PIN) == 0:
            pulse_start = time.time()

        while GPIO.input(config.DISTANCE_SENSOR_BCM_ECHO_PIN) == 1:
            pulse_end = time.time()

        pulse_duration = pulse_end - pulse_start
        distance = round(pulse_duration * config.SPEED_OF_SOUND_MM_PER_SEC/2, 0)

        return distance, pulse_duration


class BmpSensor(object):

    sensor = None

    def __init__(self):
        # For more information see
        # https://learn.adafruit.com/using-the-bmp085-with-raspberry-pi/configuring-the-pi-for-i2c and
        # https://learn.adafruit.com/using-the-bmp085-with-raspberry-pi/hooking-everything-up
        self.sensor = BMP085.BMP085()

    def get_values(self):
        temperature = self.sensor.read_temperature()
        pressure = self.sensor.read_pressure() * 0.01
        altitude = self.sensor.read_altitude()
        sealevel_pressure = self.sensor.read_sealevel_pressure() * 0.01

        return temperature, pressure, altitude, sealevel_pressure
