import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
from tornado.options import define, options
import multiprocessing
import logging
import sensors
import gps


define("port", default=80, help="run on the given port", type=int)

clients = []
input_queue = multiprocessing.Queue()
output_queue = multiprocessing.Queue()

class IndexHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render("index.tpl.html")


class ControlSocketHandler(tornado.websocket.WebSocketHandler):
    def data_received(self, chunk):
        pass

    def open(self, *args):
        logging.info("New socket connection")
        clients.append(self)

    def on_message(self, message):
        input_queue.put(message)

    def on_close(self):
        clients.remove(self)

def poll_queue():
    if not output_queue.empty():
        message = output_queue.get()
        for client in clients:
            client.write_message(message)

def main():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)-15s %(name)-12s: %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    logging.getLogger('').addHandler(console)

    sensor_poller = sensors.SensorPoller(output_queue)
    sensor_poller.daemon = True
    sensor_poller.start()

    gps_poller = gps.GpsPoller(output_queue)
    gps_poller.daemon = True
    gps_poller.start()

    app = tornado.web.Application([
        (r'/control', ControlSocketHandler),
        (r'/', IndexHandler),
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': r'./static/'})
    ])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)

    logging.info("Listening on port:", options.port)

    mainloop = tornado.ioloop.IOLoop.instance()
    scheduler_interval = 10
    scheduler = tornado.ioloop.PeriodicCallback(poll_queue, scheduler_interval, io_loop=mainloop)
    try:
        scheduler.start()
        mainloop.start()
    except (SystemExit, KeyboardInterrupt, Exception) as e:
        logging.exception(e)
        if None != sensor_poller:
            sensor_poller.close()
            sensor_poller.join(1000)

        if None != gps_poller:
            gps_poller.close()
            gps_poller.join(1000)

    logging.info("Exiting")

if __name__ == "__main__":
    main()
