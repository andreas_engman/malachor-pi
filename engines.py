import logging
import multiprocessing
import time
import PicoBorgRev

class EnginesController(multiprocessing.Process):
    """
    Input queue processor
    """
    def __init__(self, input_queue, output_queue):
        multiprocessing.Process.__init__(self)
        self.output_queue = output_queue
        self.input_queue = input_queue

        logging.info("Initializing picoborg reverse")
        self.pbr = PicoBorgRev.PicoBorgRev()
        self.pbr.Init()
        self.pbr.printFunction = logging.debug

        logging.info("Testing engines")
        time.sleep(2)
        self.pbr.SetMotor1(0.1)
        self.pbr.SetMotor2(-0.1)
        time.sleep(0.5)
        self.pbr.SetMotor1(-0.1)
        self.pbr.SetMotor2(0.1)
        time.sleep(0.5)

    def close(self):
        if None != self.pbr:
            self.pbr.SetMotors(0)
            self.pbr.MotorsOff()

    def run(self):
        while True:
            if not self.input_queue.empty():
                message = self.input_queue.get()
                if message.action == 'engine-speed':

                    engine = message.engine
                    speed = message.speed

                    if speed > 1:
                        speed = 1
                    elif speed < -1:
                        speed = -1

                    logging.info("Setting engine %s to %f", engine, speed)

                    if engine == "left":
                        self.pbr.SetMotor1(speed)
                    elif engine == "right":
                        self.pbr.SetMotor2(speed)
                    else:
                        logging.error("No such engine %s", engine)
                else:
                    logging.warning("No such action %s", message.action)
            time.sleep(0.1)
